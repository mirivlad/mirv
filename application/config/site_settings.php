<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['site_settings']['site_name'] = "M.I.R.V. CMS";
$config['site_settings']['site_enviroment'] = "dev";
$config['site_settings']['bootstrap_theme'] = "cerulean";
$config['site_settings']['site_template'] = "default";
$config['site_settings']['main_module'] = "welcome";
$config['site_settings']['migration_version'] = "20180410110000";