<?php

/*
 * MODULES AND PLUGINS LIBRARY. REQUIRE HOOKS DEFINED IN APPPATH."config/hooks.php"
 * PLUGIN - its utilites i.e. Bootstrap or TinyMCE who require to be loaded but not work with user
 * MODULE - its analog Packages in Codeigniter but work in HMVC logic.
 */

Class Mirv {

    function __construct() {
        $this->installed();
        $this->activated();
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    /*
     * INSTALL FUNCTIONS
     */

    //get installed plugins array and set into config array
    function installed() {
        $results = $this->db->get('site_plugins')->result();
        $plugins = array();
        foreach ($results as $plugin) {
            $plugins[$plugin->name] = $plugin;
        }
        $this->config->set_item('plugins', $plugins);
        return $this->config->item('plugins');
    }

    //check plugin install
    function is_install($plugin_name) {
        if (array_key_exists($plugin_name, $this->config->item('plugins'))) {
            return TRUE;
        }
        return FALSE;
    }

    //install plugin
    function install($data) {
        if ($this->is_install($data->name)) {
            return TRUE;
        } else {
            if ($this->db->insert('site_plugins', $data)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    //uninstall plugin
    function uninstall($plugin_name) {
        if (!$this->is_install($plugin_name)) {
            return TRUE;
        } else {
            $this->db->where('name', $plugin_name);
            if ($this->db->delete('site_plugins')) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /*
     * ACTIVATE FUNCTIONS
     */

    //get activated plugins and set list into config
    function activated() {
        $this->db->where('active', 1);
        $results = $this->db->get('site_plugins')->result();
        $plugins = array();
        foreach ($results as $plugin) {
            $plugins[$plugin->name] = $plugin;
        }
        $this->config->set_item('active_plugins', $plugins);
        return $this->config->item('active_plugins');
    }

    //check plugin activated
    function is_active($plugin_name) {
        if (array_key_exists($plugin_name, $this->config->item('active_plugins'))) {
            return TRUE;
        }
        return FALSE;
    }

    //activate plugin
    function set_active($plugin_name) {
        if ($this->is_install($plugin_name)) {
            $this->db->where('name', $plugin_name);
            $data = array(
                'active' => 1
            );
            if ($this->db->update('site_plugins', $data)) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
        return FALSE;
    }

    //disactivate plugin
    function unset_active($plugin_name) {
        if ($this->is_install($plugin_name)) {
            $this->db->where('name', $plugin_name);
            $data = array(
                'active' => 0
            );
            if ($this->db->update('site_plugins', $data)) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
        return FALSE;
    }

    /*
     * AUTOLOAD FUNCTIONS
     */

    //get autoload plugins and set list into config
    function autoloaded() {
        $this->db->where('autoload', 1);
        $results = $this->db->get('site_plugins')->result();
        $plugins = array();
        foreach ($results as $plugin) {
            $plugins[$plugin->name] = $plugin;
        }
        $this->config->set_item('autoload_plugins', $plugins);
        return $this->config->item('autoload_plugins');
    }

    //autoload plugin
    function autoload() {
        $plugins = $this->config->item('autoload_plugins');
        foreach ($plugins as $plugin) {
            $q = $plugin->name;
            if ($this->is_active($q)) {
                Modules::run($q);
            }
        }
    }

    //autoload plugin
    function set_autoload($plugin_name) {
        if ($this->is_install($plugin_name)) {
            $this->db->where('name', $plugin_name);
            $data = array(
                'autoload' => 1
            );
            if ($this->db->update('site_plugins', $data)) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
        return FALSE;
    }

    //unautoload plugin
    function unset_autoload($plugin_name) {
        if ($this->is_install($plugin_name)) {
            $this->db->where('name', $plugin_name);
            $data = array(
                'autoload' => 0
            );
            if ($this->db->update('site_plugins', $data)) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
        return FALSE;
    }

    /*
     * UTILITIES FUNCTIONS
     */

    //get current module name
    function current_module() {
        return $this->load->current_module();
    }
    
    function current_controller(){
        return $this->load->current_controller();
    }

    //get current module name
    function main_module() {
        return $this->settings->item('main_module');
    }

    //get avaliable modules and plugins
    function get_list() {
        $this->load->helper('directory');
        $arr = array();
        foreach ($this->config->item('modules_locations') as $path => $key) {
            $list = directory_map($path, 1);
            foreach ($list as $module) {
                $arr[] = rtrim($module, '/');
            }
        }
        return $arr;
    }

}
