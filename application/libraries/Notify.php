<?php

/**
 * Notify – класс уведомлений пользователя для Codeigniter
 *
 * Пример применения в php:
 *
 * Вывод одной ошибки или сообщения
 * $this->notify->returnError('Текст ошибки');
 *
 * Вывод нескольких сообщений
 * $this->notify->error('Случилась какая-то ошибка');
 * $this->notify->success('Но основную часть мы выполнили');
 * $this->notify->returnNotify();
 *
 * @package codeigniter-notify-library
 * @author Eduardo Kozachek <eduard.kozachek@gmail.com>
 * @version $Revision: 1.03 $
 * @access public
 * @see http://nadvoe.org.ua
 * @changed 06.08.16 17:08 by Mirivlad for M.I.R.V. CMS
 */
class Notify {

    private $notify,
	    $returnTo,
	    $ci_session,
	    $additionalData,
	    $mustDie = true,
	    $returnResult,
	    $region = 'default',
	    $ttl = 5,
	    $silent;
    private $css = "";
    private $js = '';

    /**
     * Конструктор сохраняет сессию внутри этого класса и выставляет флаги запроса
     *
     * @access public
     */
    function __construct() {
	$this->config->load('notify', true);
        $this->load->library('session');
	$this->ttl = $this->config->item('fade_timeout', 'notify');
        $this->initJsCss();
    }
    
    public function __get($var) {
        return get_instance()->$var;
    }
    /**
     * Функция выводящая сохраненные в библиотеке стили и javascript
     * Ее подключение обязательно
     *
     * @global string $this->css - стили
     * @global string $this->js - скрипты
     * @return string HTML
     * @access public
     */
    public function initJsCss() {
        $html = $this->template->widget('notify_init', ['ttl'=>$this->ttl], true);
        $this->template->add_to('header', $html);

    }

    /**
     * Основная функция возврата
     * На ней выполнение скрипта завершается
     *
     * @param string $json двумерный массив с типом сообщения и текстом
     * @global string $_SERVER['HTTP_REFERER'] | $this->returnTo - адрес предыдущей страницы
     * @uses Session Сессии codeigniter
     * @uses base_url() Функция возвращающая корневую директорию
     * @uses site_url() Функция преобразования путей приложения
     * @return array Сохраняет массив сообщений в сессию
     * @return json Выдает json-массив в javascript
     * @access public
     */
    public function returnNotify() {
	if ($this->returnTo != '') {
	    if ($this->returnTo == '/'){
		$this->returnTo = base_url();
            }elseif (strstr($this->returnTo, 'http://') or strstr($this->returnTo, 'https://')){
		$this->returnTo = $this->returnTo;
            }else{
		$this->returnTo = site_url($this->returnTo);
            }
	}

	$json = $this->notify;

	$json['data'] = $this->additionalData;

	if ($this->input->is_ajax_request()) {
	    $json['comeback'] = $this->returnTo;
	    $json = json_encode($json);
	    die($json);
	} else {
	    if ($this->returnTo == '') {
		if (isset($_SERVER['HTTP_REFERER'])){
		    $this->returnTo = $_SERVER['HTTP_REFERER'];
                }else{
		    $this->returnTo = base_url();
                }
	    }


	    $data = $this->session->userdata('notify');

	    if ($data && is_array($data)) {
		$json = array_merge($data, $json);
	    }

	    $this->session->set_userdata('notify', $json);

	    if ($this->mustDie) {
		redirect($this->returnTo);
		$this->returnTo = '';
		die();
	    }else{
		return $this->returnResult OR false;
            }
	}
    }

    /**
     * Добавление ошибки в очередь
     *
     * @param string $message - Текст сообщения
     * @param string $ttl - Время жизни, 0 - неограничено
     * @param string $region - Регион для вывода сообщения
     * @global string $this->notify - очередь сообщений
     * @access public
     */
    public function error($message, $ttl = null, $region = null) {
	if (!$this->silent) {
	    $this->notify[] = array(
		"isError" => 1,
		"type" => "alert-danger",
		"message" => $message,
		"region" => !is_null($region) ? $region : $this->region,
		"ttl" => !is_null($ttl) ? $ttl : $this->ttl
	    );
	}
    }

    /**
     * Добавление сообщения в очередь
     *
     * @param string $message - Текст сообщения
     * @param string $ttl - Время жизни, 0 - неограничено
     * @param string $region - Регион для вывода сообщения
     * @global string $this->notify - очередь сообщений
     * @access public
     */
    public function success($message, $ttl = null, $region = null) {
	if (!$this->silent) {
	    $this->notify[] = array(
		"isError" => 0,
		"type" => "alert-success",
		"message" => $message,
		"region" => !is_null($region) ? $region : $this->region,
		"ttl" => !is_null($ttl) ? $ttl : $this->ttl
	    );
	}
    }

    /**
     * Добавление сообщения в очередь и прекращение выполнение скрипта
     *
     * @param string $message - Текст сообщения
     * @param string $ttl - Время жизни, 0 - неограничено
     * @param string $region - Регион для вывода сообщения
     * @access public
     */
    public function returnError($message, $ttl = null, $region = null) {
	$this->error($message, $ttl, $region);

	$this->returnResult = false;

	return $this->returnNotify();
    }

    /**
     * Добавление сообщения в очередь и прекращение выполнение скрипта
     *
     * @param string $message - Текст сообщения
     * @param string $ttl - Время жизни, 0 - неограничено
     * @param string $region - Регион для вывода сообщения
     * @access public
     */
    public function returnSuccess($message, $ttl = null, $region = null) {
	$this->success($message, $ttl, $region);

	$this->returnResult = true;

	return $this->returnNotify();
    }

    /**
     * Добавление данных в ответ
     *
     * @param array $data - Данные
     * @global string $this->additionalData - данные
     * @access public
     */
    public function setData($data) {
	$this->additionalData = $data;
    }

    /**
     * Получение данных из ответа
     *
     * @global string $this->additionalData - данные
     * @global string $this->session - сессия
     * @access public
     */
    public function getData() {
	// уведомления текущего запроса
	if (isset($this->additionalData))
	    $additionalData = $this->additionalData;
	else {
	    $sess = $this->session->userdata('notify');

	    if (isset($sess['data']) && $sess['data']) {
		$additionalData = $sess['data'];
	    }
	    else
		$additionalData = '';
	}

	// уведомления предыдущего запроса

	return $additionalData;
    }

    /**
     * Установка адреса перенаправления
     *
     * @param string $url - URL
     * @global string $this->returnTo - URL
     * @access public
     */
    public function setComeback($url) {
	$this->returnTo = $url;
    }

    /**
     * Вывод и очистка очереди сообщений
     *
     * @global string $this->notify - Очередь сообщений
     * @uses Session CI_Session
     * @access public
     */
    public function getMessages($region = 'default') {

	// уведомления текущего запроса
	if (isset($this->notify) && is_array($this->notify) && count($this->notify))
	    $notifies = $this->notify;
	else
	    $notifies = array();

	// уведомления предыдущего запроса
	$sess = $this->session->userdata('notify');

	if (isset($sess) && is_array($sess) && count($sess))
	    $notifies = array_merge($notifies, $sess);

	// вывод
	$html = '';

	if (isset($notifies) && is_array($notifies) && count($notifies)) {
	    foreach ($notifies as $field => $n) {
		if (is_array($n) && !isset($n['type'])) {
		    foreach ($n as $key => $nn) {
			if (isset($nn['region']) && $region == $nn['region'] && isset($nn['message']) && $nn['message']) {
			    $html .= $this->template->widget('notify', $nn, true);
			    unset($notifies[$field]);
			}
		    }
		} else {
		    if (isset($n['region']) && $region == $n['region'] && isset($n['message']) && $n['message']) {
			$html .= $this->template->widget('notify', $n, true);
			unset($notifies[$field]);
		    }
		}
	    }
	}

	if (!count($notifies))
	    $this->session->unset_userdata('notify');
	else
	    $this->session->set_userdata('notify', $notifies);

	return '<div class="notify ' . $region . '">' . $html . '</div>';
    }

    /**
     * Метод для предотвращения прекращения выполнения скрипта 
     *
     * @param bool $mustDie - Включение/выключение прерывания скрипта
     * @access public
     */
    public function mustDie($mustDie = true) {
	$this->mustDie = (bool) $mustDie;
    }

    /**
     * Метод для предотвращения прекращения выполнения скрипта 
     *
     * @param bool $mustDie - Включение/выключение прерывания скрипта
     * @access public
     */
    public function setSilence($mode = false) {
	$this->silent = (bool) $mode;
    }

    /**
     * Метод для выбора места в скрипте, где именно выводить сообщения
     *
     * @param bool $mustDie - Включение/выключение прерывания скрипта
     * @access public
     */
    public function setRegion($nameRegion = 'default') {
	$this->region = $nameRegion;
    }

    /**
     * Метод установки времени жизни сообщения
     *
     * @param bool $mustDie - Включение/выключение прерывания скрипта
     * @access public
     */
    public function setTtl($ttl = 5) {
	$this->ttl = $ttl;
    }

}