<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Settings library
 */
class Settings {

    function __construct() {
        $this->load->database();
        $this->get_settings();
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    //get all settings
    function get_settings() {
        $this->config->load('site_settings');
        
        //$results = $this->db->get('site_settings')->result();
        //foreach ($results as $setting) {
        //    $this->config->set_item($setting->name, $setting->value);
        //}
    }

    //get item settings
    function item($item) {
//        $this->db->where('name', $item);
//        $data = $this->db->get('site_settings');
//        return $data->row()->value;
        //print_r($this->config->item($item,'site_settings')); die();
        return $this->config->item($item,'site_settings');
    }

    //update item settings
    function update_item($name, $value) {
        $this->load->helper('file');
        $settings = read_file(APPPATH . 'config/site_settings.php');
        $old_value = '$config[\'site_settings\'][\'' . $name . '\'] = "' . $this->item($name) . '";';
        $new_value = '$config[\'site_settings\'][\'' . $name . '\'] = "' . $value . '";';
        $settings = str_replace($old_value, $new_value, $settings);
        if (!write_file(APPPATH . 'config/site_settings.php', $settings)) {
            echo 'Unable to write the file!<br>'
            . 'Check avaliable file '.APPPATH . 'config/site_settings.php or this file permission (must be RW web server user or PHP interpreter)';
            die();
        } else {
            echo 'File written!';
        }
        
        //file_get_contents(APPPATH.'config/settings.php');
//        if (isset($name) AND $name != '') {
//            $query = $this->db->get_where('site_settings', array('name' => $name));
//            if ($query->num_rows() > 0) {
//                $this->db->where('name', $name);
//                if ($this->db->update('site_settings', array('value' => $value))) {
//                    return TRUE;
//                }
//            }
//        }
//        return FALSE;
    }

    //update all settings
    function update_settings($data) {
        foreach ($data as $name => $value) {
            if (!$this->update_item($name, $value)) {
                $this->get_settings();
                return FALSE;
            }
        }
        $this->get_settings();
        return TRUE;
    }

}
