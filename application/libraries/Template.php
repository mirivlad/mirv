<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * CMS Template library
 */
class Template {

    //Adding object containing header, body, footer and meta contents for adding from other part of system
    var $adding;
    var $page_title;
    //array with once viewed widget names
    var $widget_array;
    function __construct() {
        $this->load->library('settings');
        $this->site_template = 'templates' . DC . $this->settings->item('site_template');
        $this->module = $this->mirv->current_module();
        $this->page_title = "Title Not Found";
        $this->current_url = $this->current_url();
        

        $this->add_to('footer', $this->js('/assets/js/jquery-2.2.4.min.js'));
        $this->add_to('footer', $this->js('/assets/js/up_button.js'));
        $this->add_to('header', $this->css('/assets/css/up_button.css'));
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    function current_url() {
        $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']), 'https') === FALSE ? 'http' : 'https';
        $actual_link = $protocol . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        return $actual_link;
    }

    function add_to($part = 'header', $content = '', $once = true) {
        if (!is_array($this->adding)) {
            $this->adding = array();
        }

        if ($once) {
            if (!array_key_exists($part, $this->adding)) {
                $this->adding[$part][] = $content . PHP_EOL;
            }
            if (!in_array($content . PHP_EOL, $this->adding[$part])) {
                $this->adding[$part][] = $content . PHP_EOL;
            }
        } else {
            $this->adding[$part][] = $content . PHP_EOL;
        }
    }

    public function set_meta($meta_name, $meta_content) {
        $this->add_to('meta', '<meta name="' . $meta_name . '" content="' . $meta_content . '">');
    }

    public function set_title($string) {
        $this->add_to('title', $this->settings->item('site_name') . " :: " . $string);
    }

    public function set_pagetitle($string) {
        $this->page_title = $string;
        $this->set_title($string);
    }

    public function header() {
        if (!array_key_exists('header', $this->adding)) {
            $this->adding['header'] = array();
        }
        $string = "";
        foreach ($this->adding['header'] as $val) {
            $string .= $val . PHP_EOL;
        }
        return $string;
    }

    public function body() {
        if (!array_key_exists('body', $this->adding)) {
            $this->adding['body'] = array();
        }
        $string = "";
        foreach ($this->adding['body'] as $val) {
            $string .= $val . PHP_EOL;
        }
        return $string;
    }

    public function footer() {
        if (!array_key_exists('footer', $this->adding)) {
            $this->adding['footer'] = array();
        }
        $string = "";
        foreach ($this->adding['footer'] as $val) {
            $string .= $val . PHP_EOL;
        }
        return $string;
    }

    public function meta() {
        if (!array_key_exists('meta', $this->adding)) {
            $this->adding['meta'] = array();
        }
        $string = "";
        foreach ($this->adding['meta'] as $val) {
            $string .= $val . PHP_EOL;
        }
        return $string;
    }

    public function title() {
        $this->add_to('title', $this->settings->item('site_name'), true);
        return $this->adding['title'][0];
    }

    public function js($url) {
        return '<script type="text/javascript" src="' . $url . '"></script>' . PHP_EOL;
    }

    public function css($url) {
        return '<link rel="stylesheet" href="' . $url . '">' . PHP_EOL;
    }

    function widget($widget_name = '', $data = [], $once = false) {
        //print_r($this->settings); die();
        if (!is_array($this->widget_array) OR count($this->widget_array) == 0 ){
            $this->widget_array = array();
        }
        $widget = explode('/', $widget_name);

        if (is_array($widget) AND count($widget) > 1) {
            if ($widget[0] == "") {
                $view_path = ".." . DC . "views" . DC . "templates" . DC . $this->settings->item('site_template');
                $widget_file = $widget_name;
            } else {
                foreach ($this->config->item('modules_locations') as $key => $value) {
                    $file = $key . $widget[0] . DC . "views" . DC . "widgets" . DC . $widget[1] . ".php";
                    if (file_exists($file)) {
                        $view_path = $value . $widget[0] . DC . "views";
                        $widget_file = $widget[1];
                        break;
                    }
                }
            }
        } else {
            $view_path = ".." . DC . "views" . DC . "templates" . DC . $this->settings->item('site_template');
            $widget_file = $widget_name;
        }
        
        if (!$once OR !array_key_exists($widget_name, $this->widget_array)) {
            $var = $this->load->view($view_path . DC . "widgets" . DC . $widget_file, $data, TRUE);
            if ($once){
                $this->widget_array[$widget_name]=1;
            }
            return $var . PHP_EOL;
        } else {
            return;
        }
    }

    public function render($file, $data = '') {

        $module = $this->module;
        if ($module) {
            foreach ($this->config->item('modules_locations') as $key => $value) {
                $_path = $key . $module;
                if (file_exists($_path . DC . "views" . DC . $file . EXT)) {
                    $view_path = $value . $module . DC . "views" . DC;
                    break;
                }
            }
        } else {
            $view_path = $this->site_template . DC;
        }
        $this->current_url = $this->current_url();
        $this->page = $this->load->view($view_path . $file, $data, TRUE) . PHP_EOL;

        $this->load->view($this->site_template . DC . 'index');
    }

    public function render_ajax($file, $data = '') {

        $module = $this->module;
        if ($module) {
            foreach ($this->config->item('modules_locations') as $key => $value) {
                $_path = $key . $module;
                if (file_exists($_path . DC . "views" . DC . $file . EXT)) {
                    $view_path = $value . $module . DC . "views" . DC;
                    break;
                }
            }
        } else {
            $view_path = $this->site_template . DC;
        }
        $this->current_url = $this->current_url();
        $this->page = $this->load->view($view_path . $file, $data, TRUE) . PHP_EOL;

        $this->load->view($this->site_template . DC . 'index_ajax');
    }

}
