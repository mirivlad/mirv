<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MX_Controller {

    public function __construct() {
        //$this->lang->load('account');
        parent::__construct();
    }

    public function index() {
        $this->aauth->control('public');
        $this->template->set_pagetitle('Аккаунт');
        $this->template->render('account');
        //$this->load->view('welcome_message');
    }
    function login() {
        if ($this->input->post('login') AND !$this->aauth->is_loggedin()) {
            if ($this->aauth->login($this->input->post('user_name'), $this->input->post('user_password'), true)) {
                $this->notify->setComeback($this->template->current_url);
                $this->notify->returnSuccess($this->lang->line('account_logging_success'));
               
            } else {
                $this->notify->setComeback($this->template->current_url);
                $this->notify->returnError($this->lang->line('aauth_error_login_failed_all'));
            }
        } else {
            $this->notify->setComeback("/");
            $this->notify->returnError($this->lang->line('account_logout_success'));
        }
    }
    function logout(){
        $this->aauth->logout();
        $this->notify->setComeback("/");
        $this->notify->returnSuccess($this->lang->line('account_logging_success'));
    }
    
    public function no_access(){
        $this->template->set_pagetitle('Аккаунт');
        $this->template->render('no_access');
    }

}
