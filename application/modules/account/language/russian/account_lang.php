<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Account proceed */
$lang['account_already_loging'] = 'Вход уже выполнен!';
$lang['account_logging_success'] = 'Вы вошли в свой аккаунт.';
$lang['account_logout_success'] = 'Вы вышли из аккаунта.';
$lang['account_no_access'] = 'Извините, у вас нет прав доступа к этой странице.';
$lang['account_login_form'] = 'Вход в систему';
$lang['account_registration'] = 'Регистрация';
$lang['account_username'] = 'Пользователь';
$lang['account_password'] = 'Пароль';
$lang['account_login'] = 'Вход';
$lang['account_exit'] = 'Выход';