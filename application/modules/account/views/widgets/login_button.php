<?php
$this->lang->widget('account');
if (!$this->aauth->is_loggedin()):
?>
<button type="button" class="btn btn-default navbar-btn" data-toggle="modal" data-target="#login_form">
    <i class="icon-enter"></i> <?= $this->lang->line('account_login')?>
</button>
<?php
$form = $this->template->widget('account/login_form');
$this->template->add_to("footer", Modules::run("bootstrap/modal", "login_form", $this->lang->line('account_login_form'), $form, '', true, ""));
else: ?>
<a href="/account/logout" class="btn btn-default navbar-btn"><i class="icon-exit"></i> <?= $this->lang->line('account_exit')?></a>
<?php endif; ?>

