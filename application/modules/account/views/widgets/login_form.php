<?php
$this->load->helper('form');
$this->lang->widget('account');
echo '<a href="/account/register"><i class="icon-adduseralt"></i> '.$this->lang->line('account_registration').'</a><br><hr>';
echo form_fieldset('<i class="icon-enter"></i> '.$this->lang->line('account_login_form'));
echo form_open('account/login');
echo form_label($this->lang->line('account_username').": ", 'user_name');
echo form_input(['name'=>'user_name', 'id'=>'user_name', 'placeholder'=>$this->lang->line('account_username'), 'class'=>"form-control"])."<br>";
echo form_label($this->lang->line('account_password').": ", 'user_password');
echo form_password(['name'=>'user_password', 'id'=>'user_password', 'placeholder'=>$this->lang->line('account_password'), 'class'=>"form-control"])."<br>";
echo form_submit('login', $this->lang->line('account_login'), 'class="btn btn-primary"');
//echo form_hidden("redirect_url", $this->template->current_url());
echo form_close();
echo form_fieldset_close();

?>