<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->aauth->control('admin');
    }

    public function index() {
        $this->template->set_pagetitle('Панель администратора - Общие настройки');
        $this->template->render('index');
        //$this->load->view('welcome_message');
    }

    public function auth() {
        $this->template->set_pagetitle('Панель администратора - Аутентификация');
        $this->template->render('auth');
        //$this->load->view('welcome_message');
    }
    
//    public function migrations() {
//        $this->load->library('migration');
//        $this->template->set_pagetitle('Панель администратора - Миграции БД');
//        //$data['current_db']=$this->migration->current();
//        //$data['avaliable_db']=$this->migration->get_fs_version();
//        $this->template->render('mig');
//        //$this->load->view('welcome_message');
//    }
    
    public function create_user($email, $pass, $username = FALSE) {
        $this->aauth->create_user($email . "@example.com", $pass, $username);
    }

}
