<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migrations extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->aauth->control('admin');
        $this->load->library('migration');
    }

    public function index() {

        $this->template->set_pagetitle('Панель администратора - Миграции БД');
        $this->template->render('migrations');
    }

    public function set($id) {
        //$this->migration->current($id);
        if ($id == $this->settings->item('migration_version')) {
            $this->notify->setComeback('/admin/migrations');
            $this->notify->returnError('БД осталась на версии: ' . $this->settings->item('migration_version'));
        }
        if ($this->migration->version($id)) {
            $this->settings->update_item('migration_version', $id);
            $this->notify->setComeback('/admin/migrations');
            $this->notify->returnSuccess('БД мигрирована на версию: ' . $id);
        } else {
            $this->notify->setComeback('/admin/migrations');
            $this->notify->returnError('БД осталась на версии: ' . $this->settings->item('migration_version'));
        }
        //$this->config->set_item('migration_version', $id);
    }

}
