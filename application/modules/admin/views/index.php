<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include 'nav.php';
?>
<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>
<p>If you would like to edit this page you'll find it located at:</p>
<pre><code>application/views/templates/default/welcome.php</code></pre>
<p>The corresponding controller for this page is found at:</p>
<pre><code>application/controllers/Welcome.php</code></pre>
<h3>Docs</h3>
<p><a href="/docs/codeigniter/index.html">CodeIgniter User Guide</a></p>
<p><a href="/docs/bootstrap3/index.html">Bootstrap 3 User Guide</a></p>
<p><a href="/docs/whhg/whhg.html">WebHostyng Glyph Font</a></p>
<hr>
<h3>Utilites</h3>
<p><a href="/phpmyadmin">PhpMyAdmin</a></p>

<hr>
<pre>
<code class="language-php">
    Current Module: <?= $this->mirv->current_module(); ?>
</code>
</pre>

<br>
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modality">
    Launch demo modal
</button>
<?php
echo Modules::run("bootstrap/modal", "modality", "Тест", "Текст тестовый, обыкновенный.", "Тут могла бы быть ваша кнопка", true, "large");
?>
<br>
<br>