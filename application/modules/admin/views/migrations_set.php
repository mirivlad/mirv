<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<p>Текущая версия БД: <?=$this->config->item('migration_version')?></p>
<p>Доступные версии БД: </p>
<?php
$migrations_list = $this->migration->find_migrations();
$list_mig = array();
$this->load->library("utils");
foreach ( $migrations_list as $ver => $path) {
       $list_mig[] = "<a href='/admin/migrations/set/".$ver."' data-toggle='modal' data-target='#modality'>".$this->utils->get_classname_from_file($path).". Версия: $ver</a>";
}
echo Modules::run("bootstrap/dropdown_button","Выбор версии", $list_mig, $type = 'primary', $size = '');
?>

<pre>
<code class="language-php">
<?php

var_dump($this->migration->find_migrations());

?>
</code>
</pre>
