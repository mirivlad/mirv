<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

    public function index() {
        $this->template->set_pagetitle('Главная');
        $this->template->render('welcome');
    }

}
