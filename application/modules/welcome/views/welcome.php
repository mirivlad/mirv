<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<h3>Приветствуем в M.I.R.V. CMS!</h3>
Репозиторий проекта: <a href="https://bitbucket.org/mirivlad/mirv/">M.I.R.V. CMS</a>
<p>MIRV-CMS это система управления контентом построенная на основе:</p>
    <ul>
        <li>CodeIgniter Framework</li>
        <li>HMVC от wiredesign</li>
        <li>Библиотека авторизации aauth</li>
        <li>Иконочный шрифт - Webhosting Glyph Font</li>
        <li>WISIWIG-редактор - TinyMCE</li>
        <li>Подсветка синтаксиса - PRISM</li>
        <li>CSS-фреймворк - Twitter Bootstrap 3</li>
    </ul>

<p>Шаблон по умолчанию расположен по пути:</p>
<pre>
<code>application/views/templates/default/</code>
</pre>
<p>Содержимое страницы которую вы видите расположена по пути:</p>
<pre>
<code>application/modules/<?= $this->mirv->current_module(); ?>/views/welcome.php</code>
</pre>
<p>Контроллер отвечающий за вывод этой страницы:</p>
<pre><code>application/modules/<?= $this->mirv->current_module(); ?>/controllers/<?= $this->mirv->current_controller(); ?>.php</code></pre>
<h3>Документация</h3>
<p><a href="/docs/codeigniter/index.html">CodeIgniter User Guide</a></p>
<p><a href="/docs/bootstrap3/index.html">Bootstrap 3 User Guide</a></p>
<p><a href="/docs/whhg/whhg.html">WebHostyng Glyph Font</a></p>
<hr>
<h3>Утилиты</h3>
<p><a href="/phpmyadmin">PhpMyAdmin</a></p>
<hr>
<h3>CMS URLs</h3>
<p><a href="/admin">Admin Panel Module</a></p>
<p><a href="/account">Account Module</a></p>
<hr>
<pre>
<code class="language-php">
    Main Module: <?= $this->mirv->main_module(); ?>
</code>
<code class="language-php">
    Current Module: <?= $this->mirv->current_module(); ?>
</code>
</pre>
<pre class="language-php"><code>echo "Hello World";</code></pre>
<br>
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modality">
    Launch demo modal
</button>
<?php
echo Modules::run("bootstrap/modal", "modality", "Тест", "Текст тестовый, обыкновенный.", "Тут могла бы быть ваша кнопка", true, "large");
?>
<br>
<br>
<textarea id="textarea1" name="textarea1"></textarea>
<?=Modules::run("tinymce/set")?>
<br>
<br>