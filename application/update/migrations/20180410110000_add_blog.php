<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_blog extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'blog_id' => array(
                                'type' => 'INT',
                                'constraint' => 10,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'blog_user_id' => array(
                                'type' => 'INT',
                                'constraint' => 10,
                                'unsigned' => TRUE
                        ),
                        'blog_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
						'blog_desc' => array(
                                'type' => 'TEXT',
                                'null' => TRUE,
                        ),
                ));
                $this->dbforge->add_key('blog_id', TRUE);
				$this->dbforge->add_key('user_id');
                $this->dbforge->create_table('site_blogs');
        }

        public function down()
        {
                $this->dbforge->drop_table('site_blogs');
        }
}