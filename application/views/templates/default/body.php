<body  style="margin: 0em 2em;">
    <?= $this->notify->getMessages() ?>
    <div class="container-fluid">
        <div class="row" style="padding-bottom: 4em;">
            <div class="col-md-1"></div>
            <div class="col-md-10 col-xs-12">
                <nav class="navbar navbar-default navbar-fixed-top" style="margin-left: 5%; margin-right: 5%; width: 90%;">
                    <div class="container-fluid">
                        <div class="navbar-header pull-left">

                            <a class="navbar-brand" href="/"><img class="pull-left" style="height: 30px; margin-top: -5px; padding-right: .5em;" alt="<?= $this->config->item('site_name') ?>" src="/assets/img/mirv.png" />
                                <?= $this->config->item('site_name') ?>
                            </a>
                        </div>
                        <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="navbar-collapse collapse pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="/">Главная</a></li>
                                <li><a href="/admin">Админка</a></li>
                                <li><a href="/account">Аккаунт</a></li>
                                <li><div><?= $this->template->widget('account/login_button') ?></div></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 col-xs-12">
                <div id="content">
                    <?= $this->template->body() ?>
                    <h1><?= $this->template->page_title ?></h1>
                    <?= $this->template->page ?>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>