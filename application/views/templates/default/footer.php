<div class="container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 col-xs-12">
            <p class="footer">
                Render time: <strong><?= $this->benchmark->elapsed_time('total_execution_time_start') * 1000 ?> ms</strong><br>
                Memory usage: <strong><?= $this->benchmark->memory_usage() ?></strong><br>
                Total databse queries: <strong><?= $this->db->total_queries() ?></strong><br>
                Database queries time: <strong><?= $this->db->elapsed_time() * 1000 ?>  ms</strong><br>
                <?php echo (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?>
            </p>
            <p class="text-center">
                &copy; Copyright 2016-<?=date("Y")?> by <a class="label label-primary" href="http://mirivlad.ru">Mirivlad</a>
            </p>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<?= $this->template->footer() ?>
</body>
</html>