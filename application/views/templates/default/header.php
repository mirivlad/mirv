<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
        <?=$this->template->meta()?>
	<title><?=$this->template->title()?></title>
        <?=$this->template->header()?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
