<!--Notify-->
<style>
    /** Notification **/
    .notify
    {
        display:block;
        position:fixed;
        top:20px;
        right:20px;
        z-index: 5000;
    }
</style>
<script type="text/javascript">
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, <?=$ttl?>);

</script>
<!--Notify-->