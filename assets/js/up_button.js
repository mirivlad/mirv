$(document).ready(function() {
    $('<div id="arrow"><span class="label label-primary" style="padding: 1 0.5em; font-size: 1em;"><i class="icon-arrow-up"> <strong>Наверх</strong></i></span</div>').insertAfter('#content');
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 400) {
            $('#arrow').fadeIn();
        }
        else {
            $('#arrow').fadeOut();
        }
        ;
    });
    $('#arrow').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');
    });
});