$(document).ready(function () {
    var openPanels;
    if (typeof Cookies.get('activePanelGroup') !== 'undefined') {

        openPanels = Cookies.get('activePanelGroup').split(",");
        openPanels.forEach(function (entry) {
            $("#" + entry).addClass("in");
        });
    }

    $(".panel .panel-collapse").on('shown.bs.collapse', function () {
        var active = $(this).attr('id');
        // add to cookie when opened
        if (typeof Cookies.get('activePanelGroup') !== 'undefined') {
            Cookies.set('activePanelGroup', Cookies.get('activePanelGroup') + ',' + active);
        } else {
            Cookies.set('activePanelGroup', active);
        }
    });

    $(".panel .panel-collapse").on('hidden.bs.collapse', function () {
        var closed = $(this).attr('id');
        // delete from cookie when closed
        Cookies.set('activePanelGroup', Cookies.get('activePanelGroup').replace(closed, ""));
        // remove ',,' is found
        Cookies.set('activePanelGroup', Cookies.get('activePanelGroup').replace(",,", ""));
    });

});