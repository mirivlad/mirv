<?php

/**
 * Bootstrap plugin for MIRV_CMS
 *
 * @author mirivlad (mirivlad@gmail.com)
 */
class Bootstrap extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('bootstrap_widgets');
    }

    function index() {
        $theme = $this->settings->item('bootstrap_theme');
        $this->config->set_item('themes_path', '/plugins/bootstrap/assets/bootstrap/themes/' . $theme . '.css');
        $this->template->add_to('header', $this->template->css('/plugins/bootstrap/assets/bootstrap/themes/' . $theme . '.css'));
        $this->template->add_to('header', $this->template->css('/plugins/bootstrap/assets/whhg-font/css/whhg.css'));
        $this->template->add_to('footer', $this->template->js('/assets/js/jquery-2.2.4.min.js'));
        $this->template->add_to('footer', $this->template->js('/plugins/bootstrap/assets/bootstrap/js/bootstrap.min.js'));
    }

    function progress_bar($value, $max_value, $min_value = 0, $class = '', $type = '') {
        if (isset($class) AND $class != '') {
            $class = " progress-bar-" . $class;
        }
        $percent = round(($value * 100) / $max_value, 2);
        $data = array(
            "value" => $value,
            "max_value" => $max_value,
            "min_value" => $min_value,
            "percent" => $percent,
            "class" => $class
        );
        $widget = 'progress_bar';
        if ($type != '') {
            $widget .= '_' . $type;
        }
        return $this->template->widget('bootstrap/'.$widget, $data);
    }

    // id - id for modal header
    // title - title for modal
    // body - body text
    // footer - footer text
    // cancel - show or not cancel button
    // type - NONE or large
    function modal($id, $title = "", $body = "", $footer = "", $cancel = FALSE, $type = '') {
        if ($type == 'large') {
            $type = ' modal-lg';
        }
        $data = array(
            'id' => $id,
            'title' => $title,
            'body' => $body,
            'cancel' => $cancel,
            'footer' => $footer,
            'type' => $type
        );
        
        return $this->template->widget('bootstrap/modal', $data);
    }
    
    // footer - footer text
    // cancel - show or not cancel button
    // type - NONE or large
    function modal_ajax($footer = "", $cancel = FALSE, $type = '') {
        if ($type == 'large') {
            $type = ' modal-lg';
        }
        $data = array(
            'cancel' => $cancel,
            'footer' => $footer,
            'type' => $type
        );
        
        $add_js = $this->template->widget('bootstrap/open_modal_ajax', $data, true);
        $this->template->add_to("footer", $add_js);

        return $this->template->widget('bootstrap/modal_ajax', $data, true);
    }
    // title - title for button
    // list - array for <li> elements
    // type - primary or other boostrap state class
    // size - button size
    function dropdown_button($title = "i", $list = array(), $type = 'primary', $size = '') {
        $size = ' ' . $size . ' ';
        $data = array(
            'title' => $title,
            'list' => $list,
            'type' => $type,
            'size' => $size
        );
        return $this->template->widget('bootstrap/dropdown_button', $data);
    }

    // id - unique id for panel
    // title - text for panel header
    // body - text for panel body
    // type - primary or other boostrap state class
    // init_state - state for start state panel (true - uncollapsed, false - collapsed)
    function panel_collapse($id, $title, $body, $type = 'primary', $init_state = true) {
        if (!$init_state) {
            $state = '';
        } else {
            $state = ' on';
        }
        $data = array(
            'id' => $id,
            'title' => $title,
            'body' => $body,
            'type' => $type,
            'state' => $state,
        );

        $this->template->add_to('footer', $this->template->js('/plugins/bootstrap/assets/bootstrap/js/js.cookie.js'));
        $this->template->add_to('footer', $this->template->js('/plugins/modules/bootstrap/assets/bootstrap/js/collapse_state.js'));
        return $this->template->widget('bootstrap/panel_collapse', $data);
    }

}
