<div id="<?=$id?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="<?=$id?>Label" aria-hidden="true">
    <div class="modal-dialog<?=$type?>">
        <div class="modal-content">
        <?php
        if ($title != "") {
        ?>
            <div class = "modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only"><?=$this->lang->line('Сlose')?></span>
                </button>
                <h3 class="modal-title" id="<?=$id?>Label"><?=$title?></h3>
            </div>
        <?php
        }
        if ($body != "") {
        ?>
            <div class="modal-body">
                <?=$body?>
            </div>
        <?php
        }
        if ($footer != "" OR $cancel) {
        ?>
            <div class="modal-footer">
            <?=$footer?>
            <?php if ($cancel){ ?>
                <a role="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><?=$this->lang->line('Cancel')?></a>
            <?php } ?>    
            </div>
        <?php
        }
        ?>
        </div>
    </div>
</div>


