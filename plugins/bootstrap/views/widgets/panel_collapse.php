<div class="panel panel-<?=$type?>">
    <div class="panel-heading" data-toggle="collapse" data-target="#panel<?=$id?>">
        <h4 class="panel-title">
            <?=$title?>
        </h4>
    </div>
    <div id="panel<?=$id?>" class="panel-collapse collapse<?=$state?>">
        <div class="panel-body">
            <?=$body?>
        </div>
    </div>
</div>
