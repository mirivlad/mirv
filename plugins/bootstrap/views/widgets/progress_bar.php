<div class='progress'>
    <div class='progress-bar<?=$class?>' role='progressbar' aria-valuenow='<?=$value?>' aria-valuemin='<?=$min_value?>' aria-valuemax='<?=$max_value?>' style='width: <?=$percent?>%;'>
        <span class='sr-only'><?=$value?> <?=$this->lang->line('from')?> <?=$max_value?></span>
    </div>
</div>

