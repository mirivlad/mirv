<div class='progress'>
    <div class='progress-bar<?=$class?>' role='progressbar' aria-valuenow='<?=$value?>' aria-valuemin='<?=$min_value?>' aria-valuemax='<?=$max_value?>' style='width: <?=$percent?>%;'>
        <?=$percent?>%
    </div>
</div>

