<div class='progress'>
    <div class='progress-bar<?=$class?>' role='progressbar' aria-valuenow='<?=$value?>' aria-valuemin='<?=$min_value?>' aria-valuemax='<?=$max_value?>' style='width: <?=$percent?>%;'>
        <?=$value?> <?=$this->lang->line('from')?> <?=$max_value?>
    </div>
</div>

