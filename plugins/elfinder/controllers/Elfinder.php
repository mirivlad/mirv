<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Elfinder plugin for MIRV_CMS
 *
 * @author mirivlad (mirivlad@gmail.com)
 */
class Elfinder extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->elfinder_init();
    }

    function elfinder_init($fs_path) {

        $this->load->helper('path');
        $opts = array(
            // 'debug' => true, 
            'roots' => array(
                array(
                    'driver' => 'LocalFileSystem',
                    'path' => set_realpath($fs_path),
                    'URL' => site_url('yourfilespath') . '/'
                // more elFinder options here
                )
            )
        );
        $this->load->library('elfinder', $opts);
    }

}
