<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Elfinder {
  public function __construct($opts) 
  {
    include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elfinder/elFinderConnector.class.php';
    include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elfinder/elFinder.class.php';
    include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elfinder/elFinderVolumeDriver.class.php';
    include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elfinder/elFinderVolumeLocalFileSystem.class.php';
    $connector = new elFinderConnector(new elFinder($opts));
    $connector->run();
  }
}