<?php


/**
 * Prism codehighlight plugin for MIRV_CMS
 *
 * @author mirivlad (mirivlad@gmail.com)
 */

class Prism extends MX_Controller{
    
    public function __construct()
    {
        parent::__construct();
    }
    function index(){
        $this->template->add_to('footer', $this->template->js('/plugins/prism/assets/prism.js'));
        $this->template->add_to('footer', $this->template->css('/plugins/prism/assets/prism.css'));
        $data = array();
        $normalize = $this->template->widget('prism/prism', $data);
        $this->template->add_to('footer', $normalize);
    }
}
