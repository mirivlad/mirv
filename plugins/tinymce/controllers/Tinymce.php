<?php

/**
 * TinyMCE plugin for MIRV_CMS
 *
 * @author mirivlad (mirivlad@gmail.com)
 */
class Tinymce extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->index();
    }

    function index() {
        $this->template->add_to('footer', $this->template->js('/plugins/tinymce/assets/tinymce.min.js'));
        $this->template->add_to('footer', $this->template->js('/plugins/tinymce/assets/jquery.tinymce.min.js'));
    }

    function set($selector = 'textarea', $theme = 'modern', $width = "100%", $height = '250', $content_css = '', $plugins='', $toolbars='') {
        if ($content_css == '') {
            $content_css = $this->config->item('themes_path');
        }
        
        if (isset($plugins) AND is_array($plugins)){
            $plugins = implode(" ", $plugins);
        }
        
        if (isset($toolbars) AND is_array($toolbars)){
            $toolbars = implode(" ", $toolbars);
        }
        if ($this->mirv->is_active('prism')){
            $plugins = $plugins." codesample";
            $toolbars = $toolbars." codesample";
        }
        $data = array(
            "selector" => $selector,
            "theme" => $theme,
            "width" => $width,
            "height" => $height,
            "content_css" => $content_css,
            "plugins" =>  $plugins,
            "toolbars" => $toolbars
        );
        $init_tinymce = $this->template->widget('tinymce/tinymce', $data);
        $this->template->add_to('footer', $init_tinymce);
    }

}
