<script type="text/javascript">
    tinymce.init({
        selector: '<?=$selector?>',
        theme: '<?=$theme?>',
        width: '<?=$width?>',
        height: <?=$height?>,
        content_css: '<?=$content_css?>',
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor <?=$plugins?>'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code print preview media fullpage | forecolor backcolor emoticons <?=$toolbars?>'
    });
</script>